# server

## Configuration
Environment variable `PORT` to specify the port the server is listening to for RMP messages.
Environment variable `DATA_DIR` to specify the directory wher the server persists rooms and messages.

### Examples
    PORT=23333 DATA_DIR="./rooms" cargo run