use std::env;
use std::net::TcpListener;
use std::net::TcpStream;

mod room;
mod rmp;

use lib_rmp::message::RmpMessage;
use lib_rmp::message_type::RmpMessageType;


fn main() {
    println!("Starting Rusty Messaging Server");
    let port : String = env::var("PORT").unwrap_or(String::from("2305"));
    let listener : TcpListener = TcpListener::bind(format!("0.0.0.0:{}", port)).unwrap();
    println!("{}", listener.local_addr().unwrap());

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("Successfully listened to message: {:?}", stream);
                handle_message(stream);
            },
            Err(e) => println!("Something happened while listening for messages: {}", e),
        }
    }
}

fn handle_message(stream : TcpStream) {
    let message : RmpMessage = RmpMessage::from_stream(&stream);

    match message.message_type {
        RmpMessageType::APPEND => {
            let blob : Vec<u8> = (*message.payload).as_byte_vector();
            room::append_to_room(&message.room_key, &blob).unwrap();
        },
        RmpMessageType::FETCH => {
            println!("Fetching…");
            let blob : Vec<u8> = room::read_room(&message.room_key).unwrap();

            rmp::client::respond_with_room_messages(&stream, &message.room_key, &blob).expect("Failed to answer");
        },
    };
}