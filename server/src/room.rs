use std::env;
use std::io::Write;
use std::fs::read;
use std::fs::OpenOptions;
use std::fs::create_dir_all;

const DEFAULT_ROOMS_DIR : &str = "./rooms";
const MESSAGE_BLOB_SEPARATOR : &[u8; 11] = b"\n+++++++++\n";

pub fn read_room(room_key : &str) -> std::io::Result<Vec<u8>> {
    let room_file_path = get_room_file_path(&room_key);

    let room_blob : Vec<u8> = read(&room_file_path).unwrap_or(Vec::new());
    println!("read {} bytes from file {}", room_blob.len(), room_file_path);
    Ok(room_blob)
}

pub fn append_to_room(room_key : &str, blob : &Vec<u8>) -> std::io::Result<()> {
    let room_file_path = get_room_file_path(&room_key);

    let mut file = OpenOptions::new()
        .append(true)
        .create(true)
        .open(room_file_path)
        .unwrap();

        file.write_all(&blob)?;
        file.write_all(MESSAGE_BLOB_SEPARATOR)
}

fn get_room_file_path(room_key : &str) -> String {
    let room_directory : String = env::var("DATA_DIR").unwrap_or(String::from(DEFAULT_ROOMS_DIR));
    create_dir_all(&room_directory).unwrap();
    let room_file_path : String = format!("{}/{}.rmr", room_directory, room_key);
    println!("Room {} is located at {}", room_key, room_file_path);
    room_file_path
}
