use std::net::TcpStream;

use lib_rmp::client::send_message;
use lib_rmp::message::RmpMessage;
use lib_rmp::message_type::RmpMessageType;


pub fn respond_with_room_messages(stream : &TcpStream, room_key : &str, messages_blob : &Vec<u8>) -> std::io::Result<()> {
    let message = RmpMessage {
        message_type: RmpMessageType::FETCH,
        room_key: room_key.to_string(),
        payload: Box::new(messages_blob.to_vec()),
    };

    send_message(stream, &message)
}
