# Rusty Messaging

> Check out the live version at [https://messenger.simon-lenz.de](https://messenger.simon-lenz.de).

The Rusty Messaging application is an instant messaging system.

It uses a custom non-HTTP protocol. Not because of technical need, but after years of building web applications, I want to try out something new.

Note, that this is mainly a project to get more familiar with Rust and with networking in general. For a production service, probably none of the design decisions taken here make too much sense.

------

![Sample screnshot Home Page](./docs/screenshot-home-page-2020-11-24.png "Sample screnshot Home Page")
![Sample screnshot example Room](./docs/screenshot-2020-11-24.png "Sample screnshot example Room")

## High level overview

```mermaid
graph LR
    CC[Command line client]--RMP---S[Server]
    AC[Android client]--RMP---S
    G[HTTP-RMP Gateway]--RMP---S
    WC[Web client]--HTTP---G
```

For more details, please consult the component level READMEs linked below.

 - [Server](./server/README.md)
 - Command line client (alpha)
 - [HTTP–RMP gateway](./http-gateway/README.md) (simple HTTP server built from scratch, which talks RMP in the background)
 - [Web-client](./web-client/README.md)
 - Android client (planned)
 - lib-rmp (library which abstracts the RMP and is used in all the Rust components)

## RMP – Rusty Messaging Protocol

It is an application protocol on top of TCP.

### Message Format
```
[8bytes] type ∈ {FETCH,APPEND}
[32bytes] room-key
[4bytes] payload-size in bytes
[rest] type dependent payload
```

#### Payload of `FETCH`
##### Request
```
[0bytes] None
```    
##### Response
```
[m1 bytes] Message 1
+++++++++
[m2 bytes] Message 2
+++++++++
…
+++++++++
[mn bytes] Message m
```

#### Payload of `APPEND`
```
[m bytes] message-blob
```

### Flow
```mermaid
sequenceDiagram
  participant A as Alice
  participant S as Server
    A->>S: FETCH (room-key)
    S-->>A: FETCH (room-key, message-blobs)
    Note over A: User types message
    A->>S: APPEND (room-key, message-blob)
    A->>S: FETCH (room-key)
    S-->>A: FETCH (room-key, message-blobs)
    Note over A,S: Rinse and repeat
```

#### Initialization

Multiple Humans agree offline on a common room key (could be "Train enthusiasts" or "Alice–Bob") and a common password.
The room-key is known to the server, the password stays client side and unknown to the server.

Everybody can append message to every room, if he/she uses the wrong password, others just can't see it (Clients may hide indecipherable messages as they please).

#### Fetching messages
1. Alice calls server to fetch messages from room-key
2. Server responds with list of encrypted blobs of messages for room-key
3. Client decrypts message blobs with password
4. Alice can all read messages, where her password was correct

#### Writing messages
1. Alice [fetches messages](#Fetching_messages)
2. Alice types new message and presses send
3. Client encrypts new message and sends it to server
4. Server appends this new encrypted blob to room messages

### Improvement ideas
 - no shared password, but separate ones for each participant in a room
 - only sent messages the client doesn't already have
 - some mechanism to prevent impersonation of other users (→ do we really want authentication, though?)

## Release process

 1. execute `./bump-version.sh 1.2.3` with the desired version
 2. follow the steps this command will print out at the end

 ## Logos
![Logo version 1](./docs/logo-v1.png "Confidential message logo")
![Logo version 2](./docs/logo-v2.png "Confidential conversation logo")
