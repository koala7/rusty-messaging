import { watch } from 'vue';


export function autoScrollDown(watched, elementRef) {
    watch(
        watched, 
        () => setTimeout(() => scrollDown(elementRef.value), 0),
    );
    return {};
}

function scrollDown(element) {
    element.scrollTop = element.scrollHeight;
}