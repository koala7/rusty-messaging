export function retrieveMessages(roomKey) {
    return fetch(changeHostIfNecessary(`/api/room/${roomKey}/messages`))
        .then(response => response.json())
        .then(data => data.messages);
}

export function postMessage(roomKey, blob) {
    return fetch(changeHostIfNecessary(`/api/room/${roomKey}/messages`), {
        method: 'POST',
        body: blob,
    });
}

function changeHostIfNecessary(relativeUrl) {
    if (process.env.NODE_ENV !== "production") {
        return `http://localhost:2306${relativeUrl}`;
    }
    return relativeUrl;
}
