import sjcl from 'sjcl';


export function decrypt(blob, password) {
    const clear = sjcl.decrypt(password, blob);
    return JSON.parse(clear);
}

export function encrypt(message, password) {
    const encrypted = sjcl.encrypt(password, JSON.stringify(message));
    return encrypted;
}
