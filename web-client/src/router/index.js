import { createRouter, createWebHistory } from "vue-router";
import ARoom from "../components/a-room.vue";
import TheHomePage from "../components/the-home-page.vue";

const routes = [
    {
        path: "/",
        component: TheHomePage,
    },
    {
        path: "/room/:room_key",
        component: ARoom,
        props: true,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
