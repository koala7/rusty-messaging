#! /bin/sh
echo "In case you see problems with bumping the Rust versions, make sure you installed https://github.com/wraithan/cargo-bump correctly."

tag=${1?Please specify a version to bump to! Usage: \`./bump-version.sh 1.2.3\`}
echo "Bumping version to: $tag"

echo "Bump web-client…"
(cd web-client; npm version $tag)
echo "Done"

echo "Bump lib-rmp…"
(cd lib-rmp; cargo bump $tag; cargo check)
echo "Done"

echo "Bump server…"
(cd server; cargo bump $tag; cargo check)
echo "Done"

echo "Bump gateway…"
(cd http-gateway; cargo bump $tag; cargo check)
echo "Done"

echo "Bump command-lin-client…"
(cd command-line-client; cargo bump $tag; cargo check)
echo "Done"

echo "Version bump finished. Please"
echo " 1. commit those changes"
echo "    git commit -am 'Bump to version $tag'"
echo " 2. create git tag"
echo "    git tag -a $tag"
echo " 3. Push master branch and tag"
echo "    git push origin master --tags"
echo " 4. CI pipeline will automatically create docker images and deploy to VPS for the TAG run."
