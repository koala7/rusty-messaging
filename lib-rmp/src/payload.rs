pub trait RmpPayload {
    fn as_byte_vector(&self) -> Vec<u8>;
}

impl RmpPayload for &[u8; 7] {
    fn as_byte_vector(&self) -> std::vec::Vec<u8> { return self.to_vec(); }
}

impl RmpPayload for Vec<u8> {
    fn as_byte_vector(&self) -> std::vec::Vec<u8> { return self.to_vec(); }
}
