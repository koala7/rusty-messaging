use std::net::TcpStream;
use std::io::Read;
use std::convert::TryInto;
use std::str::from_utf8;

use super::message_type::RmpMessageType;
use super::payload::RmpPayload;


pub struct RmpMessage {
    pub message_type: RmpMessageType,
    pub room_key: String,
    pub payload: Box<dyn RmpPayload>,
}

impl RmpMessage {
    pub fn from_stream(mut stream : &TcpStream) -> Self {
        let mut message_type_bytes : [u8; 8] = [0; 8];
        let mut room_key_bytes : [u8; 32] = [0; 32];
        let mut payload_length_bytes : [u8; 4] = [0; 4];
        stream.read_exact(&mut message_type_bytes).unwrap();
        stream.read_exact(&mut room_key_bytes).unwrap();
        stream.read_exact(&mut payload_length_bytes).unwrap();

        let payload_length : u32 = u32::from_be_bytes(payload_length_bytes);
        println!("payload size in bytes: {}", payload_length);
        let mut payload_bytes : Vec<u8> = vec![0; payload_length.try_into().unwrap()];
        stream.read_exact(&mut payload_bytes).unwrap();

        let message_type_str = from_utf8(&message_type_bytes).unwrap().trim();
        let room_key_str = from_utf8(&room_key_bytes).unwrap().trim();

        println!("Deserialized message:");
        println!("Message type: {:?}", message_type_str);
        println!("Room key: {:?}", room_key_str);
        println!("Payload: {:?}", payload_bytes);

        Self {
            message_type: RmpMessageType::from_str(message_type_str),
            room_key: room_key_str.to_string(),
            payload: Box::new(payload_bytes),
        }
    }
}
