use std::io::Write;

use super::payload::RmpPayload;
use super::util::force_bytes_length;


pub struct RoomMessage {
    pub author: String,
    pub message: String,
}

impl RmpPayload for RoomMessage {
    fn as_byte_vector(&self) -> std::vec::Vec<u8> {
        let mut buffer = Vec::new();
        buffer.write(&force_bytes_length(self.author.to_string(), 32)).unwrap();
        buffer.write(self.message.as_bytes()).unwrap();
        return buffer;
    }
}
