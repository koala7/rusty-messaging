use std::str::from_utf8;
use std::iter::Iterator;

use super::message::RmpMessage;

// TODO: copied from room.rs in server package. 
pub const MESSAGE_BLOB_SEPARATOR : &str = "\n+++++++++\n";

pub struct RoomDto {
    pub room_key: String,
    pub messages: Vec<Vec<u8>>,
}

pub fn room_response_deserializer(message : RmpMessage, room_key : &str) -> RoomDto {
    let response_bytes : Vec<u8> = (*message.payload).as_byte_vector();
    let response_as_string : &str = from_utf8(&response_bytes).unwrap();
    let message_strings : Vec<&str> = response_as_string.split_terminator(&MESSAGE_BLOB_SEPARATOR).collect();
    let message_blobs : Vec<Vec<u8>> = message_strings.iter().map(|b| b.as_bytes().to_vec()).collect();
    RoomDto {
        room_key: room_key.to_string(),
        messages: message_blobs,
    }
}