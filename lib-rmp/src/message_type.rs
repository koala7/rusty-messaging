pub enum RmpMessageType {
    FETCH,
    APPEND,
}

impl RmpMessageType {
    pub fn from_str(name : &str) -> Self {
        match name {
            "APPEND" => RmpMessageType::APPEND,
            "FETCH" => RmpMessageType::FETCH,
            _ => panic!("Unknown message type {} found.", name),
        }
    }
    
    pub fn name(&self) -> &str {
        match *self {
            RmpMessageType::FETCH => "FETCH",
            RmpMessageType::APPEND => "APPEND",
        }
    }
}
