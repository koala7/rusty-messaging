use std::net::TcpStream;
use std::io::Write;

use super::util::force_bytes_length;
use super::message::RmpMessage;
use super::room_message::RoomMessage;
use super::message_type::RmpMessageType;

use super::room_utils::{RoomDto,room_response_deserializer};

pub fn fetch_room(stream : &TcpStream, room_key : &str) -> std::io::Result<RoomDto> {
    let message = RmpMessage {
        message_type: RmpMessageType::FETCH,
        room_key: room_key.to_string(),
        payload: Box::new(Vec::new()),
    };
    send_message(&stream, &message)?;
    println!("Requested messages of room {}. Now waiting for response.", room_key);
    let response : RmpMessage = RmpMessage::from_stream(&stream);
    Ok(room_response_deserializer(response, &room_key))
}

// TODO: Should be executed on client side! So not for the HTTP-RMP gateway for example
pub fn send_message_to_room(stream : &TcpStream, message : &str, author : &str, room_key : &str) -> std::io::Result<()> {
    let room_message = RoomMessage {
        message: message.to_string(),
        author: author.to_string(),
    };

    let rmp_message = RmpMessage {
        message_type: RmpMessageType::APPEND,
        room_key: room_key.to_string(),
        payload: Box::new(room_message),
    };

    send_message(&stream, &rmp_message)
}

pub fn send_message(mut stream : &TcpStream, message : &RmpMessage) -> std::io::Result<()> {
    println!("Send message to stream: {:?}", stream);

    let payload_bytes : &[u8] = &message.payload.as_byte_vector();
    let payload_size_in_bytes : u32 = payload_bytes.len() as u32;
    println!("payload has {} bytes", payload_size_in_bytes);

    stream.write(&force_bytes_length(message.message_type.name().to_string(), 8))?;
    stream.write(&force_bytes_length(message.room_key.to_string(), 32))?;
    stream.write(&payload_size_in_bytes.to_be_bytes())?;
    stream.write(&payload_bytes)?;
    stream.flush()?;
    Ok(())
}
