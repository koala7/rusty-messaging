pub fn force_bytes_length(string : String, desired_length : usize) -> Vec<u8> {
    let mut result : Vec<u8> = string.as_bytes().to_vec();
    result.resize(desired_length, 32);
    result
}
