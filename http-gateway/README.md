# HTTP–RMP Gateway

Trabslates HTTP requests to RMP. For more info consult [project level README](../README.md).

## Configuration
 - Environment variable `PORT` to specify the port the web server is listening to for HTTP messages.
 - Environment variable `RMPSERVER` to specify the addres of the RMP server.

For example `PORT=2306 RMPSERVER=0.0.0.0:23333 cargo run`

## Specification

### Request
```
GET /api/room/<room_key>/messages
```

### Response
```
200 OK
{
    messages: [
        <encrypted blob 1>,
        <encrypted blob 2>,
        …
        <encrypted blob n>,
    ]
}
```

### Request
```
POST /api/room/<room_key>/messages
<encrypted blob>
```

### Response
```
201 Created
```