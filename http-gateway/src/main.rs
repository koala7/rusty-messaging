use std::env;
use std::net::TcpStream;

use lib_rmp::client::fetch_room;
use lib_rmp::client::send_message;
use lib_rmp::message::RmpMessage;
use lib_rmp::message_type::RmpMessageType;
use lib_rmp::room_utils::RoomDto;

mod webserver;

use webserver::WebServer;

fn main() {
    println!("Starting Rusty Messaging HTTP→RMP gateway");

    let webserver_address : String = String::from(
        format!(
            "0.0.0.0:{}", 
            env::var("WEBSERVER_PORT").unwrap_or(String::from("2306"))
        ));

    WebServer {
        address: webserver_address,
        get_handler: handler_get,
        post_handler: handler_post,
    }.start();
}

fn handler_post(url : &str, message : Vec<u8>) {
    let room_key = extract_room_key(url);

    let rmp_message = RmpMessage {
        message_type: RmpMessageType::APPEND,
        room_key: room_key.to_string(),
        payload: Box::new(message),
    };
    let rmp_stream : TcpStream = TcpStream::connect(get_rmp_server_address()).unwrap();
    send_message(&rmp_stream, &rmp_message).unwrap();
}

fn handler_get(url : &str) -> Vec<u8> {
    let room_key = extract_room_key(url);
    let rmp_stream : TcpStream = TcpStream::connect(get_rmp_server_address()).unwrap();
    let room : RoomDto = fetch_room(&rmp_stream, room_key).unwrap();
    room_dto_as_json_bytes(&room)
}

fn get_rmp_server_address() -> String {
    env::var("RMPSERVER").unwrap_or(String::from("0.0.0.0:2305"))
}

fn extract_room_key(url : &str) -> &str {
    println!("was get request to url {}", url);
    let parts : Vec<&str> = url.split("/").collect();
    if parts[2] == "room" && parts[4] == "messages" {
        return parts[3];
    }
    panic!("Could not extract room key. TODO: Should not really panic, but result in BadRequest or NotFound…")
}

fn room_dto_as_json_bytes(room_dto : &RoomDto) -> Vec<u8> {
    let mut messages_json_array_bytes : Vec<u8> = Vec::new();
    messages_json_array_bytes.append(&mut b"{\n".to_vec());
    messages_json_array_bytes.append(&mut b"    \"messages\": [\n".to_vec());

    for message in &room_dto.messages {
        let mut json_encoded_message = escape_quotes(&message.to_vec());
        // let json_encoded_message = str::replace(message, "\"", "\\\"");
        messages_json_array_bytes.append(&mut b"      \"".to_vec());
        messages_json_array_bytes.append(&mut json_encoded_message);
        messages_json_array_bytes.append(&mut b"\",\n".to_vec());
    }
    if &room_dto.messages.len() > &0 {
        messages_json_array_bytes.pop();
        messages_json_array_bytes.pop();
        messages_json_array_bytes.push(b'\n');
    }

    messages_json_array_bytes.append(&mut b"    ]\n".to_vec());
    messages_json_array_bytes.append(&mut b"}\n".to_vec());

    messages_json_array_bytes
}

fn escape_quotes(input : &Vec<u8>) -> Vec<u8> {
    let mut escaped = Vec::new();
    for byte in input {
        if byte == &b'"' {
            escaped.push(b'\\');
            escaped.push(b'"');
        }
        else {
            escaped.push(byte.to_owned());
        }
    }
    escaped
}
