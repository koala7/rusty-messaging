use std::net::TcpListener;
use std::net::TcpStream;
use std::io::{BufReader, BufRead};
use std::io::Read;
use std::io::Write;

pub struct WebServer {
    pub address : String,
    pub get_handler: fn(&str) -> Vec<u8>,
    pub post_handler: fn(&str, Vec<u8>),
}

impl WebServer {
    pub fn start(&self) {
        let listener : TcpListener = TcpListener::bind(&self.address).unwrap();
        println!("Webserver listening at {}", listener.local_addr().unwrap());
    
        for stream in listener.incoming() {
            match stream {
                Ok(stream) => {
                    self.handle_connection(stream);
                },
                Err(e) => println!("Something happened while listening: {}", e),
            }
        }
    }

    fn handle_connection(&self, mut stream : TcpStream) {
        //    The normal procedure for parsing an HTTP message is to read the
        //    start-line into a structure, read each header field into a hash table
        //    by field name until the empty line, and then use the parsed data to
        //    determine if a message body is expected.  If a message body has been
        //    indicated, then it is read as a stream until an amount of octets
        //    equal to the message body length is read or the connection is closed.
    
        // startline
        // header1 CRLF
        // header2 CRLF
        // CRLF
        // (maybe body)
    
        let mut reader = BufReader::new(&stream);
        let mut http_status_line : String = String::new();
        
        reader.read_line(&mut http_status_line).unwrap();
        
        let mut http_body_length = 0;
    
        println!("Got request with status line: {}", http_status_line);
        loop {
            let mut http_header : String = String::new();
            reader.read_line(&mut http_header).unwrap();
            http_header = http_header.trim().to_string();
    
            if http_header.len() == 0 {
                break;
            }
    
            if http_header.contains("Content-Length") {
                let parts : Vec<&str> = http_header.split(':').collect();
                let number_part : &str = parts.get(1).unwrap().trim();
                let number_as_usize : usize = usize::from_str_radix(number_part, 10).unwrap();
                http_body_length = number_as_usize;
                println!("content length is {}", http_body_length);
            }
        }
        
        let http_body : Option<Vec<u8>> = if http_body_length > 0 {
            let mut body_buf = vec![0; http_body_length];
            reader.read_exact(&mut body_buf).unwrap();
            println!("body is\n{:?}", body_buf);
            Some(body_buf)
        }
        else {
            None
        };
        
        let mut split = http_status_line.split_whitespace();
        let http_method = split.next();
        let http_url = split.next();
        let http_version = split.next();
        println!("method is {:?}", http_method);
        println!("http_url is {:?}", http_url);
        println!("http_version is {:?}", http_version);
        match http_method {
            Some("GET") => {
                let response_data = (self.get_handler)(http_url.unwrap());
                let response_header = format!(
                    "HTTP/1.1 200 OK\r\nContent-Length: {}\r\nContent-Type: application/json\r\nAccess-Control-Allow-Origin: *\r\n\r\n",
                    response_data.len()
                );
                stream.write(response_header.as_bytes()).unwrap();
                stream.write(&response_data).unwrap();
            },
            Some("POST") => {
                (self.post_handler)(http_url.unwrap(), http_body.unwrap());
                stream.write(b"HTTP/1.1 201 Created\r\n").unwrap();
            },
            Some("OPTIONS") => {
                stream.write(b"HTTP/1.1 204 No Content\r\n").unwrap();
                stream.write(b"Access-Control-Allow-Headers: content-type\r\n").unwrap();
                stream.write(b"Access-Control-Allow-Origin: http://localhost:8080\r\n").unwrap();
                stream.write(b"\r\n").unwrap();
            },
            _ => println!("was unsupported request"),
        }
    
        stream.write(b"HTTP/1.1 200 OK\r\n\r\n").unwrap();
    }
}
