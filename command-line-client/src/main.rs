mod rmp;
use std::env;


fn main() {
    println!("Starting Rusty Messaging System's command line client");
    parse_args();
}

fn parse_args() {
    let args: Vec<String> = env::args().collect();
    let command = &args[1];

    match command.as_str() {
        "fetch" => {
            let room_key = &args[2];
            println!("fetching…");
            rmp::client::fetch_and_print_room(&room_key).expect("Failed to fetch room");
        },
        "send" => {
            let room_key = &args[2];
            let author = &args[3];
            let message = &args[4];
            println!("sending");
            rmp::client::send_message_to_room(&message, author, &room_key).expect("Failed to send ATTACH msg");
        }
        _ => println!("unknown command…"),
    };

    
}