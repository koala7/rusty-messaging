use std::net::TcpStream;
use std::str::from_utf8;

use lib_rmp::client::RoomDto;

pub fn fetch_and_print_room(room_key : &str) -> std::io::Result<RoomDto> {
    let stream : TcpStream = TcpStream::connect("127.0.0.1:2305")?;
    let room_data : RoomDto = lib_rmp::client::fetch_room(&stream, room_key).unwrap();
    let messages: Vec<&str> = room_data.messages.iter().map(|m| from_utf8(m).unwrap()).collect();

    println!(
        "RoomData:\n\troom: {}\n\tmessages: {:?}", 
        room_data.room_key, 
        messages
    );

    Ok(room_data)
}

pub fn send_message_to_room(message : &str, author : &str, room_key : &str) -> std::io::Result<()> {
    let stream : TcpStream = TcpStream::connect("127.0.0.1:2305")?;
    lib_rmp::client::send_message_to_room(&stream, &message, &author, &room_key)
}
