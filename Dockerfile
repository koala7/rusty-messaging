FROM nginx:1.19.4-alpine AS web-client
COPY ./web-client/dist /usr/share/nginx/html
COPY ./web-client/nginx/0-rusty-messaging.conf /etc/nginx/conf.d/

FROM debian:stretch-20201012-slim AS server
COPY ./server/target/release/rusty-messaging-server /usr/local/bin/
ENTRYPOINT ["rusty-messaging-server"]

FROM debian:stretch-20201012-slim AS http-gateway
COPY ./http-gateway/target/release/http-gateway /usr/local/bin/
ENTRYPOINT ["http-gateway"]
